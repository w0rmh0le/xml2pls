# xml2pls

## Overview

Simple bash script to parse an atom/rss xml feed into a pls playlist.

Originally created to be used for converting [YouTube](https://www.youtube.com)
playlists from [Invidious](https://www.invidio.us) into .pls files that can be
opened with your video player of choice.

## Dependencies

**xml2** - This program assumes xml2 is in your PATH

## Usage

`xml2pls [--youtube] </path/to/xml-feed>`

If you don't supply the `--youtube` option, links created will direct to
invidio.us

If you don't supply proper arguments or files, something bad might happen,
and you can only blame yourself.

The playlist that inspired this project is in the `test-data` directory in its
unprocessed xml form. Feel free to use it to test out the program.
Despite the name of the playlist, I don't really think it contains the best
bluegrass songs of all times; but it does have some decent ones.
Tracks 3 and 4 are my favorite :)
